openapi: 3.0.0
info:
  title: Midgard Public API
  version: 1.0.0-oas3
  contact:
    email: devs@thorchain.org
  description: The Midgard Public API queries THORChain and any chains linked via the Bifröst and prepares information about the network to be readily available for public users. The API parses transaction event data from THORChain and stores them in a time-series database to make time-dependent queries easy. Midgard does not hold critical information. To interact with BEPSwap and Asgardex, users should query THORChain directly.
paths:
  "/v1/account/{address}":
    get:
      operationId: GetAccount
      summary: Get an account.
      description: Gets account metadata for an address.
      parameters:
        - in: path
          name: address
          description: The account address to query
          required: true
          schema:
            type: string
          example: 'bnb19nc5gaaxfyxt7c798gzsa8pns5vgfz3fx2c6kx'
      responses:
        "200":
          $ref: '#/components/responses/AccountDetailedResponse'

  "/v1/markets":
    get:
      operationId: GetPairs
      summary: Get market pairs.
      description: Gets the list of market pairs that have been listed.
      parameters: 
        - in: query
          name: limit
          required: false
          schema:
            type: integer
            format: int32
        - in: query
          name: offset
          required: false
          schema:
            type: integer
            format: int32
      responses:
        "200":
          $ref: '#/components/responses/MarketDetailResponse'
        
  
  "/v1/depth":
    get:
      operationId: GetDepth
      summary: Get the order book.
      description: Gets the order book depth data for a given pair symbol.
      parameters:
        - in: query
          name: symbol
          description: Market pair symbol
          required: true
          schema:
            type: string
          example: RUNE-B1A_BNB
        - in: query
          name: limit
          description: The limit of results
          required: false
          schema:
            type: integer
            format: int32
          example: '[5, 10, 20, 50, 100, 500, 1000]'
      responses:
        "200":
          $ref: '#/components/responses/DepthDetailedResponse'
  
  "/v1/orders/closed":
    get:
      operationId: GetClosedOrders
      summary: Get closed orders.
      description: Gets closed (filled and cancelled) orders for a given address.
      parameters:
        - in: query
          name: address
          description: the owner address
          required: true
          schema:
            type: string
            example: 'bnb19nc5gaaxfyxt7c798gzsa8pns5vgfz3fx2c6kx'
        - in: query
          name: start
          description: start time in miliseconds
          required: false
          schema:
            type: integer
            format: int64
        - in: query
          name: end
          description: end time in miliseconds
          required: false
          schema:
            type: integer
            format: int64
        - in: query
          name: limit
          description: default 500; max 1000
          required: false
          schema:
            type: integer
            format: int32
        - in: query
          name: offset
          description: start with 0, default 0
          required: false
          schema:
            type: integer
            format: int32
        - in: query
          name: side
          description: order side. 1 for buy and 2 for sell
          required: false
          schema:
            type: integer
            format: int32
        - in: query
          name: symbol
          description: Market pair symbol
          required: false
          schema:
            type: string
          example: RUNE-B1A_BNB
      responses:
        "200":
          $ref: '#/components/responses/ClosedOrderDetailedResponse'
          
  "/v1/orders/open":
    get:
      operationId: GetOpenOrders
      summary: Get open orders.
      description: Get open orders.
      parameters:
        - in: query
          name: address
          description: the owner address
          required: true
          schema:
            type: string
            example: 'bnb19nc5gaaxfyxt7c798gzsa8pns5vgfz3fx2c6kx'
        - in: query
          name: limit
          description: default 500; max 1000
          required: false
          schema:
            type: integer
            format: int32
        - in: query
          name: offset
          description: start with 0, default 0
          required: false
          schema:
            type: integer
            format: int32
        - in: query
          name: symbol
          description: Market pair symbol
          required: false
          schema:
            type: string
          example: RUNE-B1A_BNB
      responses:
        "200":
          $ref: '#/components/responses/ClosedOrderDetailedResponse'
      
components:
  responses:
    AccountDetailedResponse:
      description: object containing account metadata for an address
      content:
        application/json:
          schema:
            $ref : '#/components/schemas/AccountDetail'
            
    DepthDetailedResponse:
      description: Gets the order book depth data for a given pair symbol.
      content:
        application/json:
          schema:
            $ref : '#/components/schemas/DepthDetail'
  
    ClosedOrderDetailedResponse:
      description: Gets closed orders for a given address
      content:
        application/json:
          schema:
            $ref : '#/components/schemas/OrderDetail'
    
    MarketDetailResponse:
      description: Gets the list of market pairs that have been listed.
      content:
        application/json:
          schema:
            $ref : '#/components/schemas/MarketDetail'
        
  schemas:
    balances:
      properties:
        symbol:
          type: string
        free:
          type: string
        locked:
          type: string
        frozen:
          type: string
      
    AccountDetail:
      type: object
      properties:
        account_number:
          type: integer
          format: int32
        address:
          type: string
        balances:
          type: array
          items:
            $ref: '#/components/schemas/balances'
        public_key:
          type: integer
        sequence:
          type: integer
          format: in64
          
    asks:
      type: array
      items:
        type: string
    
    bids:
      type: array
      items:
        type: string
        
    DepthDetail:
      properties:
        asks:
            $ref: '#/components/schemas/asks'
        bids:
            $ref: '#/components/schemas/bids'
            
    order:
      properties:
        cumulateQuantity:
          type: string
        fee:
          type: string
        lastExecutedPrice:
          type: string
        orderCreateTime:
          type: string
        orderId:
          type: string
        owner:
          type: string
        price:
          type: string
        quantity:
          type: string
        side:
          type: integer
          format: int32
        status:
          type: string
          enum:  [Ack, PartialFill, IocNoFill, FullyFill, Canceled, Expired, FailedBlocking, FailedMatching, IocExpire]
        tradeId:
          type: string
        symbol:
          type: string
        transactionHash:
          type: string
        transactionTIme:
          type: string
        type:
          type: integer
          format: int32
          
    OrderDetail:
      properties:
        order:
          type: array
          items:
           $ref: '#/components/schemas/order'
       
    market:
      properties:
        base_asset_symbol:
          type: string
        quote_asset_symbol:
          type: string
        list_price:
          type: string
        tick_size:
          type: string
        lot_size:
          type: string
  
    MarketDetail:
      type: array
      items:
        $ref: '#/components/schemas/market'
  
          
servers:
  - url: https://dex.binance.org/api
