# AccountDetail

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**account_number** | **int** |  | [optional] 
**address** | **str** |  | [optional] 
**balances** | [**list[Balances]**](Balances.md) |  | [optional] 
**public_key** | **int** |  | [optional] 
**sequence** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

