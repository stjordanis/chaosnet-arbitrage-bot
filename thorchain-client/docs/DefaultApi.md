# swagger_client.DefaultApi

All URIs are relative to *http://18.159.173.48:1317*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_thor_chain_pool**](DefaultApi.md#get_thor_chain_pool) | **GET** /thorchain/pool/{pool} | Get Pool
[**get_thor_chain_pools**](DefaultApi.md#get_thor_chain_pools) | **GET** /thorchain/pools | Get Pools
[**get_thorchain_proxied_constants**](DefaultApi.md#get_thorchain_proxied_constants) | **GET** /thorchain/constants | Get the Proxied THORChain Constants
[**get_thorchain_proxied_endpoints**](DefaultApi.md#get_thorchain_proxied_endpoints) | **GET** /thorchain/pool_addresses | Get the Proxied Pool Addresses
[**get_thorchain_proxied_lastblock**](DefaultApi.md#get_thorchain_proxied_lastblock) | **GET** /thorchain/lastblock | Get the Proxied THORChain Lastblock
[**get_tx_details**](DefaultApi.md#get_tx_details) | **GET** /thorchain/tx/{tx} | Get details of a tx by address, asset or tx-id

# **get_thor_chain_pool**
> PoolDetail get_thor_chain_pool(pool)

Get Pool

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.DefaultApi()
pool = 'pool_example' # str | pool name

try:
    # Get Pool
    api_response = api_instance.get_thor_chain_pool(pool)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_thor_chain_pool: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **pool** | **str**| pool name | 

### Return type

[**PoolDetail**](PoolDetail.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_thor_chain_pools**
> list[PoolDetail] get_thor_chain_pools()

Get Pools

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.DefaultApi()

try:
    # Get Pools
    api_response = api_instance.get_thor_chain_pools()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_thor_chain_pools: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**list[PoolDetail]**](PoolDetail.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_thorchain_proxied_constants**
> ThorchainConstants get_thorchain_proxied_constants()

Get the Proxied THORChain Constants

Returns a proxied endpoint for the constants endpoint from a local thornode

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.DefaultApi()

try:
    # Get the Proxied THORChain Constants
    api_response = api_instance.get_thorchain_proxied_constants()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_thorchain_proxied_constants: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ThorchainConstants**](ThorchainConstants.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_thorchain_proxied_endpoints**
> ThorchainEndpoints get_thorchain_proxied_endpoints()

Get the Proxied Pool Addresses

Returns a proxied endpoint for the pool_addresses endpoint from a local thornode

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.DefaultApi()

try:
    # Get the Proxied Pool Addresses
    api_response = api_instance.get_thorchain_proxied_endpoints()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_thorchain_proxied_endpoints: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ThorchainEndpoints**](ThorchainEndpoints.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_thorchain_proxied_lastblock**
> ThorchainLastblock get_thorchain_proxied_lastblock()

Get the Proxied THORChain Lastblock

Returns a proxied endpoint for the lastblock endpoint from a local thornode

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.DefaultApi()

try:
    # Get the Proxied THORChain Lastblock
    api_response = api_instance.get_thorchain_proxied_lastblock()
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_thorchain_proxied_lastblock: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

[**ThorchainLastblock**](ThorchainLastblock.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

# **get_tx_details**
> TxDetails get_tx_details(tx)

Get details of a tx by address, asset or tx-id

Return an array containing the event details

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.DefaultApi()
tx = 'tx_example' # str | ID of in tx

try:
    # Get details of a tx by address, asset or tx-id
    api_response = api_instance.get_tx_details(tx)
    pprint(api_response)
except ApiException as e:
    print("Exception when calling DefaultApi->get_tx_details: %s\n" % e)
```

### Parameters

Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
 **tx** | **str**| ID of in tx | 

### Return type

[**TxDetails**](TxDetails.md)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

