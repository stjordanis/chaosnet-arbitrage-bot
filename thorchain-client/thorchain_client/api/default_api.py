# coding: utf-8

"""
    ThorNode Query API

    ThorNode Query API  # noqa: E501

    OpenAPI spec version: 1.0.0-oas3
    Contact: zheye@umich.edu
    Generated by: https://github.com/swagger-api/swagger-codegen.git
"""

from __future__ import absolute_import

import re  # noqa: F401

# python 2 and python 3 compatibility library
import six

from thorchain_client.api_client import ApiClient


class DefaultApi(object):
    """NOTE: This class is auto generated by the swagger code generator program.

    Do not edit the class manually.
    Ref: https://github.com/swagger-api/swagger-codegen
    """

    def __init__(self, api_client=None):
        if api_client is None:
            api_client = ApiClient()
        self.api_client = api_client

    def get_thor_chain_pool(self, pool, **kwargs):  # noqa: E501
        """Get Pool  # noqa: E501

        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.get_thor_chain_pool(pool, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param str pool: pool name (required)
        :return: PoolDetail
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async_req'):
            return self.get_thor_chain_pool_with_http_info(pool, **kwargs)  # noqa: E501
        else:
            (data) = self.get_thor_chain_pool_with_http_info(pool, **kwargs)  # noqa: E501
            return data

    def get_thor_chain_pool_with_http_info(self, pool, **kwargs):  # noqa: E501
        """Get Pool  # noqa: E501

        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.get_thor_chain_pool_with_http_info(pool, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param str pool: pool name (required)
        :return: PoolDetail
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['pool']  # noqa: E501
        all_params.append('async_req')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in six.iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_thor_chain_pool" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'pool' is set
        if ('pool' not in params or
                params['pool'] is None):
            raise ValueError("Missing the required parameter `pool` when calling `get_thor_chain_pool`")  # noqa: E501

        collection_formats = {}

        path_params = {}
        if 'pool' in params:
            path_params['pool'] = params['pool']  # noqa: E501

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.select_header_accept(
            ['application/json'])  # noqa: E501

        # Authentication setting
        auth_settings = []  # noqa: E501

        return self.api_client.call_api(
            '/thorchain/pool/{pool}', 'GET',
            path_params,
            query_params,
            header_params,
            body=body_params,
            post_params=form_params,
            files=local_var_files,
            response_type='PoolDetail',  # noqa: E501
            auth_settings=auth_settings,
            async_req=params.get('async_req'),
            _return_http_data_only=params.get('_return_http_data_only'),
            _preload_content=params.get('_preload_content', True),
            _request_timeout=params.get('_request_timeout'),
            collection_formats=collection_formats)

    def get_thor_chain_pools(self, **kwargs):  # noqa: E501
        """Get Pools  # noqa: E501

        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.get_thor_chain_pools(async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :return: list[PoolDetail]
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async_req'):
            return self.get_thor_chain_pools_with_http_info(**kwargs)  # noqa: E501
        else:
            (data) = self.get_thor_chain_pools_with_http_info(**kwargs)  # noqa: E501
            return data

    def get_thor_chain_pools_with_http_info(self, **kwargs):  # noqa: E501
        """Get Pools  # noqa: E501

        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.get_thor_chain_pools_with_http_info(async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :return: list[PoolDetail]
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = []  # noqa: E501
        all_params.append('async_req')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in six.iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_thor_chain_pools" % key
                )
            params[key] = val
        del params['kwargs']

        collection_formats = {}

        path_params = {}

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.select_header_accept(
            ['application/json'])  # noqa: E501

        # Authentication setting
        auth_settings = []  # noqa: E501

        return self.api_client.call_api(
            '/thorchain/pools', 'GET',
            path_params,
            query_params,
            header_params,
            body=body_params,
            post_params=form_params,
            files=local_var_files,
            response_type='list[PoolDetail]',  # noqa: E501
            auth_settings=auth_settings,
            async_req=params.get('async_req'),
            _return_http_data_only=params.get('_return_http_data_only'),
            _preload_content=params.get('_preload_content', True),
            _request_timeout=params.get('_request_timeout'),
            collection_formats=collection_formats)

    def get_thorchain_proxied_constants(self, **kwargs):  # noqa: E501
        """Get the Proxied THORChain Constants  # noqa: E501

        Returns a proxied endpoint for the constants endpoint from a local thornode  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.get_thorchain_proxied_constants(async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :return: ThorchainConstants
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async_req'):
            return self.get_thorchain_proxied_constants_with_http_info(**kwargs)  # noqa: E501
        else:
            (data) = self.get_thorchain_proxied_constants_with_http_info(**kwargs)  # noqa: E501
            return data

    def get_thorchain_proxied_constants_with_http_info(self, **kwargs):  # noqa: E501
        """Get the Proxied THORChain Constants  # noqa: E501

        Returns a proxied endpoint for the constants endpoint from a local thornode  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.get_thorchain_proxied_constants_with_http_info(async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :return: ThorchainConstants
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = []  # noqa: E501
        all_params.append('async_req')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in six.iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_thorchain_proxied_constants" % key
                )
            params[key] = val
        del params['kwargs']

        collection_formats = {}

        path_params = {}

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.select_header_accept(
            ['application/json'])  # noqa: E501

        # Authentication setting
        auth_settings = []  # noqa: E501

        return self.api_client.call_api(
            '/thorchain/constants', 'GET',
            path_params,
            query_params,
            header_params,
            body=body_params,
            post_params=form_params,
            files=local_var_files,
            response_type='ThorchainConstants',  # noqa: E501
            auth_settings=auth_settings,
            async_req=params.get('async_req'),
            _return_http_data_only=params.get('_return_http_data_only'),
            _preload_content=params.get('_preload_content', True),
            _request_timeout=params.get('_request_timeout'),
            collection_formats=collection_formats)

    def get_thorchain_proxied_endpoints(self, **kwargs):  # noqa: E501
        """Get the Proxied Pool Addresses  # noqa: E501

        Returns a proxied endpoint for the pool_addresses endpoint from a local thornode  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.get_thorchain_proxied_endpoints(async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :return: ThorchainEndpoints
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async_req'):
            return self.get_thorchain_proxied_endpoints_with_http_info(**kwargs)  # noqa: E501
        else:
            (data) = self.get_thorchain_proxied_endpoints_with_http_info(**kwargs)  # noqa: E501
            return data

    def get_thorchain_proxied_endpoints_with_http_info(self, **kwargs):  # noqa: E501
        """Get the Proxied Pool Addresses  # noqa: E501

        Returns a proxied endpoint for the pool_addresses endpoint from a local thornode  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.get_thorchain_proxied_endpoints_with_http_info(async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :return: ThorchainEndpoints
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = []  # noqa: E501
        all_params.append('async_req')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in six.iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_thorchain_proxied_endpoints" % key
                )
            params[key] = val
        del params['kwargs']

        collection_formats = {}

        path_params = {}

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.select_header_accept(
            ['application/json'])  # noqa: E501

        # Authentication setting
        auth_settings = []  # noqa: E501

        return self.api_client.call_api(
            '/thorchain/pool_addresses', 'GET',
            path_params,
            query_params,
            header_params,
            body=body_params,
            post_params=form_params,
            files=local_var_files,
            response_type='ThorchainEndpoints',  # noqa: E501
            auth_settings=auth_settings,
            async_req=params.get('async_req'),
            _return_http_data_only=params.get('_return_http_data_only'),
            _preload_content=params.get('_preload_content', True),
            _request_timeout=params.get('_request_timeout'),
            collection_formats=collection_formats)

    def get_thorchain_proxied_lastblock(self, **kwargs):  # noqa: E501
        """Get the Proxied THORChain Lastblock  # noqa: E501

        Returns a proxied endpoint for the lastblock endpoint from a local thornode  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.get_thorchain_proxied_lastblock(async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :return: ThorchainLastblock
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async_req'):
            return self.get_thorchain_proxied_lastblock_with_http_info(**kwargs)  # noqa: E501
        else:
            (data) = self.get_thorchain_proxied_lastblock_with_http_info(**kwargs)  # noqa: E501
            return data

    def get_thorchain_proxied_lastblock_with_http_info(self, **kwargs):  # noqa: E501
        """Get the Proxied THORChain Lastblock  # noqa: E501

        Returns a proxied endpoint for the lastblock endpoint from a local thornode  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.get_thorchain_proxied_lastblock_with_http_info(async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :return: ThorchainLastblock
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = []  # noqa: E501
        all_params.append('async_req')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in six.iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_thorchain_proxied_lastblock" % key
                )
            params[key] = val
        del params['kwargs']

        collection_formats = {}

        path_params = {}

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.select_header_accept(
            ['application/json'])  # noqa: E501

        # Authentication setting
        auth_settings = []  # noqa: E501

        return self.api_client.call_api(
            '/thorchain/lastblock', 'GET',
            path_params,
            query_params,
            header_params,
            body=body_params,
            post_params=form_params,
            files=local_var_files,
            response_type='ThorchainLastblock',  # noqa: E501
            auth_settings=auth_settings,
            async_req=params.get('async_req'),
            _return_http_data_only=params.get('_return_http_data_only'),
            _preload_content=params.get('_preload_content', True),
            _request_timeout=params.get('_request_timeout'),
            collection_formats=collection_formats)

    def get_tx_details(self, tx, **kwargs):  # noqa: E501
        """Get details of a tx by address, asset or tx-id  # noqa: E501

        Return an array containing the event details  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.get_tx_details(tx, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param str tx: ID of in tx (required)
        :return: TxDetails
                 If the method is called asynchronously,
                 returns the request thread.
        """
        kwargs['_return_http_data_only'] = True
        if kwargs.get('async_req'):
            return self.get_tx_details_with_http_info(tx, **kwargs)  # noqa: E501
        else:
            (data) = self.get_tx_details_with_http_info(tx, **kwargs)  # noqa: E501
            return data

    def get_tx_details_with_http_info(self, tx, **kwargs):  # noqa: E501
        """Get details of a tx by address, asset or tx-id  # noqa: E501

        Return an array containing the event details  # noqa: E501
        This method makes a synchronous HTTP request by default. To make an
        asynchronous HTTP request, please pass async_req=True
        >>> thread = api.get_tx_details_with_http_info(tx, async_req=True)
        >>> result = thread.get()

        :param async_req bool
        :param str tx: ID of in tx (required)
        :return: TxDetails
                 If the method is called asynchronously,
                 returns the request thread.
        """

        all_params = ['tx']  # noqa: E501
        all_params.append('async_req')
        all_params.append('_return_http_data_only')
        all_params.append('_preload_content')
        all_params.append('_request_timeout')

        params = locals()
        for key, val in six.iteritems(params['kwargs']):
            if key not in all_params:
                raise TypeError(
                    "Got an unexpected keyword argument '%s'"
                    " to method get_tx_details" % key
                )
            params[key] = val
        del params['kwargs']
        # verify the required parameter 'tx' is set
        if ('tx' not in params or
                params['tx'] is None):
            raise ValueError("Missing the required parameter `tx` when calling `get_tx_details`")  # noqa: E501

        collection_formats = {}

        path_params = {}
        if 'tx' in params:
            path_params['tx'] = params['tx']  # noqa: E501

        query_params = []

        header_params = {}

        form_params = []
        local_var_files = {}

        body_params = None
        # HTTP header `Accept`
        header_params['Accept'] = self.api_client.select_header_accept(
            ['application/json'])  # noqa: E501

        # Authentication setting
        auth_settings = []  # noqa: E501

        return self.api_client.call_api(
            '/thorchain/tx/{tx}', 'GET',
            path_params,
            query_params,
            header_params,
            body=body_params,
            post_params=form_params,
            files=local_var_files,
            response_type='TxDetails',  # noqa: E501
            auth_settings=auth_settings,
            async_req=params.get('async_req'),
            _return_http_data_only=params.get('_return_http_data_only'),
            _preload_content=params.get('_preload_content', True),
            _request_timeout=params.get('_request_timeout'),
            collection_formats=collection_formats)
