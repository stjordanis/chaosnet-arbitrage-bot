# -*- coding: future_fstrings -*-
import time
import thorchain_client
from logger import get_logger, logging
from thorchain_client.rest import ApiException


thorchain_logger = get_logger("thorchain", level=logging.INFO)


class ThorApi:
    def __init__(self, host=None):
        self.api_instance = thorchain_client.DefaultApi()
        if host:
            self.api_instance.api_client.configuration.host = host
        thorchain_logger.info(f'thor api conncted to node: '
                            f'{self.api_instance.api_client.configuration.host}')

    def get_pools(self):
        """ get a list of pools with basic balance and status info"""
        try:
            pools = self.api_instance.get_thor_chain_pools()
            thorchain_logger.debug(f'available pools {pools}')
            return pools
        except ApiException as e:
            thorchain_logger.info(f'exception calling get_pools: {e}')

    def get_pool(self, pool):
        """ get basic information for input {pool}"""
        try:
            pool = self.api_instance.get_thor_chain_pool(pool=pool)
            thorchain_logger.info(f'pool info: {pool}')
            return pool
        except ApiException as e:
            if e.reason == 'Too Many Requests' or 'Not Found':
                thorchain_logger.info(f'exception: exceeded rate limit: {e}')
                time.sleep(5)
                thorchain_logger.info('calling get_pool again')
                return self.get_pool(pool=pool)
            thorchain_logger.info(f'exception calling get_pool: {e}')

    def get_rune_price(self, pool):
        """ get balance_rune / balance_asset for input {pool}"""
        try:
            pool = self.api_instance.get_thor_chain_pool(pool=pool)
            rune_price = float(pool.balance_rune) / float(pool.balance_asset)
            thorchain_logger.debug(f'rune price of {pool}: {rune_price}')
            return rune_price
        except ApiException as e:
            thorchain_logger.info(f'exception calling get_pool: {e}')

    def get_tx_in(self, hash):
        """ get tx_in for input {hash}"""
        try:
            tx_in = self.api_instance.get_tx_details(tx=hash)
            thorchain_logger.debug(f'tx_in: {tx_in}')
            return tx_in
        except ApiException as e:
            thorchain_logger.info(f'exception calling get_tx_in: {e}')

    def get_tx_status(self, hash, timeout=500):
        for tick in range(0, timeout):
            try:
                time.sleep(1)
                tx_in = self.api_instance.get_tx_details(tx=hash)
                if tx_in:
                    if tx_in.status == 'done':
                        thorchain_logger.info(f'operation {tx_in.tx.memo} successful '
                                            f'tx_in: {tx_in} ')
                        return True
            except ApiException as e:
                if e.reason == 'Too Many Requests' or 'Not FOund':
                    time.sleep(5)
                    thorchain_logger.info('exception: exceeded rate limit')
                else:
                    thorchain_logger.info(f'exception calling get_tx_status: {e}')
        thorchain_logger.info('time out')
        return False

    def get_vault(self, chain=None):
        try:
            address_info = self.api_instance.get_thorchain_proxied_endpoints().current
            if chain:
                address_info = list(filter(lambda info: info.chain == chain, address_info))[0]
                thorchain_logger.debug(f'address info: {address_info}')
                return address_info.address
            thorchain_logger.debug(f'address info: {address_info}')
            return address_info
        except ApiException as e:
            thorchain_logger.info(f'exception calling get_vault: {e}')