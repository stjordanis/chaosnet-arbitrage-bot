# -*- coding: future_fstrings -*-
import chaosnet_client
import time
from logger import get_logger, logging
from chaosnet_client.rest import ApiException
from binance_chain.http import HttpApiClient
from binance_chain.messages import Transfer, TransferMsg
from binance_chain.wallet import Wallet


midgard_logger = get_logger("midgard", level=logging.DEBUG)


class MidgardApi:
    def __init__(self, host=None):
        self.api_instance = chaosnet_client.DefaultApi()
        if host:
            self.api_instance.api_client.configuration.host = host
        midgard_logger.info(f'Midgard connected to node: '
                            f'{self.api_instance.api_client.configuration.host}')
        try:
            midgard_logger.info(f'network health: {self.api_instance.get_health()}')
        except ApiException as e:
            midgard_logger.info(f'exception calling get_health() {e}')

    def health_check(self):
        try:
            return self.api_instance.get_health().catching_up
        except ApiException as e:
            midgard_logger.info(f'exception calling get_health() {e}')

    def get_stats(self):
        try:
            stats = self.api_instance.get_stats()
            midgard_logger.info(f'midgard stats: {stats}')
            return stats
        except ApiException as e:
            midgard_logger.info(f'exception calling get_stats() {e}')

    def get_assets(self, assets):
        try:
            assets = self.api_instance.get_asset_info(assets)
            midgard_logger.debug(f'asset stats: {assets}')
            return assets
        except ApiException as e:
            midgard_logger.info(f'exception calling get_asset_info() {e}')

    def get_pools(self):
        try:
            pools = self.api_instance.get_pools()
            midgard_logger.info(f'available pools: {pools}')
            return pools
        except ApiException as e:
            midgard_logger.info(f'exception calling get_pools() {e}')

    def get_pools_detail(self, assets):
        try:
            pools_detail = self.api_instance.get_pools_details(asset=assets)
            midgard_logger.debug(f'pools detail: {pools_detail}')
            return pools_detail
        except ApiException as e:
            midgard_logger.info(f'exception calling get_pools_details() {e}')

    def get_pools_address(self, chain=None):
        try:
            pools_address = self.api_instance.get_thorchain_proxied_endpoints().current
            if chain:
                chain_filtered = list(filter(lambda pool: pool.chain == chain, pools_address))[0]
                midgard_logger.info(f'{chain_filtered}')
                return chain_filtered.address
            midgard_logger.info(f'address: {pools_address}')
            return pools_address
        except ApiException as e:
            midgard_logger.info(f'exception calling get_thorchain_proxied_endpoints() {e}')

    def get_txs_id(self, txid, timeout=300):
        for tick in range(0, timeout):
            try:
                time.sleep(1)
                tx = self.api_instance.get_tx_details(txid=txid, offset=0, limit=1)
                if tx.count > 0:
                    if tx.txs[0].status.lower() != 'pending':
                        midgard_logger.debug(f'tx info: {tx.txs[0]}')
                        midgard_logger.info(f'operation {tx.txs[0].type} successful '
                                            f'in: {tx.txs[0]._in.coins} '
                                             f'out: {"".join(str(outs.coins) for outs in tx.txs[0].out)}')
                        return tx.txs[0]
            except ApiException as e:
                midgard_logger.info(f'exception calling get_tx_details() {e}')

    def get_txs_type(self, type, offset, limit):
        try:
            tx = self.api_instance.get_tx_details(type=type, offset=offset, limit=limit)
            #midgard_logger.info(f'{type} {offset} {limit} transaction: {tx}')
            return tx.txs
        except ApiException as e:
            midgard_logger.info(f'exception calling get_tx_details() {e}')

    def get_txs_count_type(self, type):
        try:
            tx = self.api_instance.get_tx_details(type=type, offset=0, limit=1)
            return tx.count
        except ApiException as e:
            midgard_logger.info(f'exception calling get_tx_details() {e}')

    def get_txs_address(self, address):
        tx = self.api_instance.get_tx_details(address=address, offset=0, limit=10)
        return tx

    def get_txs_asset(self, asset):
        tx = self.api_instance.get_tx_details(asset=asset, offset=0, limit=5)
        midgard_logger.info(f'tx info: {tx}')
        return tx

    def get_txs_latest(self, offset, limit):
        tx = self.api_instance.get_tx_details(offset=offset,limit=limit)
        # midgard_logger.debug(f'tx info: {tx}')
        return tx

    def get_total_tx(self):
        try:
            total_tx = self.api_instance.get_stats().total_tx
            midgard_logger.info(f'total tx: {total_tx}')
            return int(total_tx)
        except ApiException as e:
            midgard_logger.info(f'exception calling get_stats() {e}')

    def get_staker(self, address=None, assets=None):
        try:
            if address and assets:
                staker_pools_data = self.api_instance.get_stakers_address_and_asset_data(address=address, asset=assets)
                midgard_logger.info(f'staker pool data : {staker_pools_data}')
                return staker_pools_data
            elif address:
                staker_address_data = self.api_instance.get_stakers_address_data(address=address)
                midgard_logger.info(f'staker address data : {staker_address_data} ')
                return staker_address_data
            else:
                staker_data = self.api_instance.get_stakers_data()
                midgard_logger.info(f'stakers: {staker_data}')
                return staker_data
        except ApiException as e:
            midgard_logger.info(f'exception calling get_staker() {e}')

    def get_network(self):
        network_info = self.api_instance.get_network_data()
        midgard_logger.info(f'network: {network_info}')

    def get_nodes(self):
        node_info = self.api_instance.get_nodes()
        midgard_logger.info(f'nodes: {node_info}')

    def get_constants(self):
        constant_info = self.api_instance.get_thorchain_proxied_constants()
        midgard_logger.info(f'constants: {constant_info}')
