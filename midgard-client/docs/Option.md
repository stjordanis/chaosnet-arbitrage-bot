# Option

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**price_target** | **str** |  | [optional] 
**withdraw_basis_points** | **str** |  | [optional] 
**asymmetry** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

