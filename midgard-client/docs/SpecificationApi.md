# swagger_client.SpecificationApi

All URIs are relative to *http://18.159.173.48:8080*

Method | HTTP request | Description
------------- | ------------- | -------------
[**get_swagger**](SpecificationApi.md#get_swagger) | **GET** /v1/swagger.json | Get Swagger

# **get_swagger**
> get_swagger()

Get Swagger

Returns human and machine readable swagger/openapi specification.

### Example
```python
from __future__ import print_function
import time
import swagger_client
from swagger_client.rest import ApiException
from pprint import pprint

# create an instance of the API class
api_instance = swagger_client.SpecificationApi()

try:
    # Get Swagger
    api_instance.get_swagger()
except ApiException as e:
    print("Exception when calling SpecificationApi->get_swagger: %s\n" % e)
```

### Parameters
This endpoint does not need any parameter.

### Return type

void (empty response body)

### Authorization

No authorization required

### HTTP request headers

 - **Content-Type**: Not defined
 - **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

