# StakersAddressData

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**pools_array** | [**list[Asset]**](Asset.md) |  | [optional] 
**total_staked** | **str** | Total staked (in RUNE) across all pools. | [optional] 
**total_earned** | **str** | Total value of earnings (in RUNE) across all pools. | [optional] 
**total_roi** | **str** | Average of all pool ROIs. | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

