# TotalVolChanges

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**time** | **int** |  | [optional] 
**buy_volume** | **str** |  | [optional] 
**sell_volume** | **str** |  | [optional] 
**total_volume** | **str** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

