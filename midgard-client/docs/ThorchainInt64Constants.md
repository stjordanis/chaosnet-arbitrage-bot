# ThorchainInt64Constants

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**bad_validator_rate** | **int** |  | [optional] 
**blocks_per_year** | **int** |  | [optional] 
**desire_validator_set** | **int** |  | [optional] 
**double_sign_max_age** | **int** |  | [optional] 
**emission_curve** | **int** |  | [optional] 
**fail_key_sign_slash_points** | **int** |  | [optional] 
**fail_keygen_slash_points** | **int** |  | [optional] 
**fund_migration_interval** | **int** |  | [optional] 
**jail_time_keygen** | **int** |  | [optional] 
**jail_time_keysign** | **int** |  | [optional] 
**lack_of_observation_penalty** | **int** |  | [optional] 
**minimum_bond_in_rune** | **int** |  | [optional] 
**minimum_nodes_for_bft** | **int** |  | [optional] 
**minimum_nodes_for_yggdrasil** | **int** |  | [optional] 
**new_pool_cycle** | **int** |  | [optional] 
**observe_slash_points** | **int** |  | [optional] 
**old_validator_rate** | **int** |  | [optional] 
**rotate_per_block_height** | **int** |  | [optional] 
**rotate_retry_blocks** | **int** |  | [optional] 
**signing_transaction_period** | **int** |  | [optional] 
**stake_lock_up_blocks** | **int** |  | [optional] 
**transaction_fee** | **int** |  | [optional] 
**validator_rotate_in_num_before_full** | **int** |  | [optional] 
**validator_rotate_num_after_full** | **int** |  | [optional] 
**validator_rotate_out_num_before_full** | **int** |  | [optional] 
**white_list_gas_asset** | **int** |  | [optional] 
**ygg_fund_limit** | **int** |  | [optional] 

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)

